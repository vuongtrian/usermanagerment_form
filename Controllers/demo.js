//Tinh diện tích tam giác d + r
function cong(a, b, callback) {
  setTimeout(function () {
    callback(a + b);
  }, 1000);
}

function nhan(a, b, callback) {
  setTimeout(function () {
    callback(a * b);
  }, 1000);
}

function chia(a, b, callback) {
  setTimeout(function () {
    callback(a / b);
  }, 1000);
}

function tinhDienTich(a, b, h, callback) {
  cong(a, b, function (result1) {
    nhan(result1, h, function (result2) {
      chia(result2, 2, function (result3) {
        callback(result3);
      });
    });
  });
}

tinhDienTich(5, 3, 6, function (result) {
  console.log("Dien tich callback", result);
});

function congPromise(a, b) {
  return new Promise(function (resolve, reject) {
      setTimeout(function(){
        resolve(a + b);
      })
    
  });
}

function nhanPromise(a, b) {
  return new Promise(function (resolve, reject) {
    resolve(a * b);
  });
}

function chiaPromise(a, b) {
  return new Promise(function (resolve, reject) {
    resolve(a / b);
  });
}

function tinhDienTichPromise(a, b, h) {
  return congPromise(a, b)
    .then(function (result1) {
      return nhanPromise(result1, h);
    })
    .then(function (result2) {
      return chiaPromise(result2, 2);
    })
    .then(function (result3) {
      return result3;
    });
}

tinhDienTichPromise (4, 3, 7).then(function(result){
    console.log("Dien tich Promise", result);
})
