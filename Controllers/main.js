document
  .getElementById("btnThemNguoiDung")
  .addEventListener("click", openthemNguoiDung);

function openthemNguoiDung() {
  document.getElementsByClassName("modal-footer")[0].innerHTML = `
        <button class = "btn btn-success" onclick = "xuLyThemNguoiDung()">Thêm</button>
    `;
}

function xuLyThemNguoiDung() {
  const hoTen = document.getElementById("HoTen").value;
  const taiKhoan = document.getElementById("TaiKhoan").value;
  const matKhau = document.getElementById("MatKhau").value;
  const email = document.getElementById("Email").value;
  const soDienThoai = document.getElementById("SoDienThoai").value;
  const loaiNguoiDung = document.getElementById("loaiNguoiDung").value;

  const nguoiDung = new NguoiDung(
    hoTen,
    email,
    taiKhoan,
    matKhau,
    soDienThoai,
    loaiNguoiDung
  );

  themNguoiDung(nguoiDung).then(function (result1) {
    xuLyLayDanhSachNguoiDung();
  });
}

// setTimeout(xuLyTimeOut, 3000);

// function xuLyTimeOut() {
//   console.log("1");
// }

var danhSachNguoiDung = [];

function xuLyLayDanhSachNguoiDung() {
  layDanhSachNguoiDung().then(function (result) {
    danhSachNguoiDung = result.data;
    renderHTML();
  });
}

const promise = layDanhSachNguoiDung();

promise.then(function (result) {
  danhSachNguoiDung = result.data;
  //   console.log(danhSachNguoiDung);
  renderHTML();
});

xuLyLayDanhSachNguoiDung();

// function renderHTML() {
const renderHTML = function(arr) {
  arr = arr || danhSachNguoiDung;
  var htmlContent = "";
  for (var i = 0; i < arr.length; i++) {
    var nguoiDung = arr[i];
    console.log(nguoiDung)
    htmlContent += `
            <tr>
                <td>${i + 1}</td>
                <td>${nguoiDung.taiKhoan}</td>
                <td>${nguoiDung.matKhau}</td>
                <td>${nguoiDung.hoTen}</td>
                <td>${nguoiDung.email}</td>
                <td>${nguoiDung.soDienThoai}</td>
                <td>${nguoiDung.loaiNguoiDung}</td>
                <td>
                    <button class ="btn btn-success data edit-user" data-toggle="modal" data-target="#myModal" data-id="${
                      nguoiDung.id
                    }">Sửa</button>
                    <button class ="btn btn-danger" onclick="xuLyXoaNguoiDung(${
                      nguoiDung.id
                    })">Xóa</button>
                </td>  
            </tr>
        `;
  }
  document.getElementById("tblDanhSachNguoiDung").innerHTML = htmlContent;
};

function xuLyXoaNguoiDung(id) {
  xoaNguoiDung(id).then(function () {
    xuLyLayDanhSachNguoiDung();
  });
}

document
  .getElementById("tblDanhSachNguoiDung")
  .addEventListener("click", handleClickEdit);

function handleClickEdit(event) {
  console.log(event.target);
  const selected = event.target;
  const id = selected.getAttribute("data-id");
  if (id) {
    document.getElementsByClassName("modal-footer")[0].innerHTML = `
    <button class = "btn btn-success" onclick = "xuLySuaNguoiDung(${id})" data-dismiss="modal">Sửa</button>
`;
    layThongTinNguoiDung(id).then(function (result) {
      const nguoiDung = result.data;

      document.getElementById("HoTen").valu = nguoiDung.hoTen;
      document.getElementById("TaiKhoan").value = nguoiDung.taiKhoan;
      document.getElementById("MatKhau").value = nguoiDung.matKhau;
      document.getElementById("Email").value = nguoiDung.email;
      document.getElementById("SoDienThoai").value = nguoiDung.soDienThoai;
      document.getElementById("loaiNguoiDung").value = nguoiDung.loaiNguoiDung;
    });
  };
};

function xuLySuaNguoiDung(id) {
  const hoTen = document.getElementById("HoTen").value;
  const taiKhoan = document.getElementById("TaiKhoan").value;
  const matKhau = document.getElementById("MatKhau").value;
  const email = document.getElementById("Email").value;
  const soDienThoai = document.getElementById("SoDienThoai").value;
  const loaiNguoiDung = document.getElementById("loaiNguoiDung").value;

  const nguoiDung = new NguoiDung(
    hoTen,
    email,
    taiKhoan,
    matKhau,
    soDienThoai,
    loaiNguoiDung
  );

  capNhatNguoiDung(id, nguoiDung).then(function () {
    // Sau khi cập nhật thành công, gọi lại hàm xuLyLayDanhSachNguoiDung
    xuLyLayDanhSachNguoiDung();
  });
}

//Chức năng tìm trong danh sách
const findUser = function () {
  const foundUser = [];
  const keyword = document
    .getElementById("txtSearch")
    .value.trim()
    .toLowerCase();
  for (var j = 0; j < danhSachNguoiDung.length; j++) {
    const currentUser = danhSachNguoiDung[j];
    var hoTen = currentUser.hoTen.toLowerCase();
    if (currentUser.taiKhoan === keyword) {
      foundUser.push(currentUser);
      break;
    }
    if (hoTen.indexOf(keyword) !== -1) {
      foundUser.push(currentUser);
    }
  }
  renderHTML(foundUser);
};


